SUMMARY = "OpenBMC IPL status observer"
DESCRIPTION = "OpenBMC IPL status observer."
PR = "r1"

inherit skeleton-python
inherit obmc-phosphor-dbus-service

VIRTUAL-RUNTIME_skeleton_workbook ?= ""

RDEPENDS_${PN} += "\
        python-dbus \
        python-json \
        python-subprocess \
        python-pygobject \
        pyphosphor \
        pyphosphor-dbus \
        ${VIRTUAL-RUNTIME_skeleton_workbook} \
        pdbg \
        "

SKELETON_DIR = "pyiplobserver"

DBUS_SERVICE_${PN} += "org.openbmc.status.IPL.service"
