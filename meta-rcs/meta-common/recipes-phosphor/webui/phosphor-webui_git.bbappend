FILESEXTRAPATHS_prepend := "${THISDIR}/phosphor-webui:"
SRC_URI += "file://logo.svg"
SRC_URI += "file://favicon.ico"
SRC_URI += "file://login-logo-size.patch"

do_compile_prepend() {
    cp -r ${WORKDIR}/logo.svg ${S}/app/assets/images/
    cp -r ${WORKDIR}/favicon.ico ${S}/app/assets/images/
}
