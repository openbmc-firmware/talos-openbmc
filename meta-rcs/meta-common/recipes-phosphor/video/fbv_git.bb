DESCRIPTION = "Frame Buffer Viewer"
LICENSE = "GPL"
LIC_FILES_CHKSUM = "file://COPYING;md5=130f9d9dddfebd2c6ff59165f066e41c"
DEPENDS = "libpng jpeg"

FILESEXTRAPATHS_prepend := "${THISDIR}/fbv_git:"

SRC_URI += "git://scm.raptorcs.com/scm/git/fbv;protocol=https"
SRCREV = "e0bfd6744c82dd0fd0fea9aa80fb0aafc361c327"

CFLAGS += "-D_GNU_SOURCE -D__KERNEL_STRICT_NAMES"
LDFLAGS += "-lpng"

S = "${WORKDIR}/git"

do_configure() {
	CC="${CC}" ./configure
}

do_compile() {
	oe_runmake CC="${CC}"
}

do_install() {
	install -d ${D}${bindir}
	install -m 0755 fbv ${D}${bindir}

# man
	install -d ${D}${mandir}/man1/
	install -m 0644 fbv.1 ${D}${mandir}/man1/fbv.1
}
